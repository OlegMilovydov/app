var humidity = document.getElementById('humidity');
var pressure = document.getElementById('pressure');
var temperature = document.getElementById('temperature');
var windSpeed = document.getElementById('wind-speed');
var weatherSummary = document.getElementById('weather-summary');
var getWeatherButton = document.getElementById('get-weather');
var inputCity = document.querySelector('#current-weather-details input');
var showCity = document.getElementById('get-weather-city');
var command = [ 'go' , 'weather' , 'do it'];


function getLocationCoords() {
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      getWeatherData(position.coords.latitude, position.coords.longitude);
    })
  } else {
    alert('Your browser does not support Navigator API');
  }
}

 function getWeatherData(latitude, longitude) {
  fetch('http://api.openweathermap.org/data/2.5/weather?lat=' + latitude + '&lon=' + longitude + '&appid=08d1316ba8742c08076e7425c28c2614')
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      displayData(data);
    })
}

function displayData(data) {
    temperature.innerText = kToC(data.main.temp) + ' C';
    humidity.innerText = data.main.humidity + '%';
    pressure.innerText = data.main.pressure + ' mmHg.';
    windSpeed.innerText = data.wind.speed + ' м/с';
    generalWeather = data.weather[0].main;
    console.log(window);
  }


function kToC(k) {
  return Math.round(k - 273);
}
function getIndicatorsCity() {
  var city = inputCity.value;
  if(city){
       fetch('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=b4f2447b3b05c86e015a35a9e833b87a')
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        displayData(data);
      })
  } else{
    alert("Please, enter city")
  }
}

showCity.addEventListener('click', getIndicatorsCity);
getWeatherButton.addEventListener('click', getLocationCoords);

